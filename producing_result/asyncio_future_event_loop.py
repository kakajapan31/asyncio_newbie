# Thay vì dùng await thì có thể dùng event_loop để thay thế

import asyncio


def mark_done(future, result):
    print('setting future result to {!r}'.format(result))  # 3
    future.set_result(result)


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        all_done = asyncio.Future()

        print('scheduling mark_done')  # 1
        event_loop.call_soon(mark_done, all_done, 'the result')

        print('entering event loop')  # 2
        result = event_loop.run_until_complete(all_done)
        print('returned result: {!r}'.format(result))  # 4
    finally:
        print('closing event loop')  # 5
        event_loop.close()
    print('future result: {!r}'.format(all_done.result()))  # 6

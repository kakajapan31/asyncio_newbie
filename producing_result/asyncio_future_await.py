import asyncio


def mark_done(future, result):
    print('setting future result to {!r}'.format(result))  # 2
    future.set_result(result)  # lưu result trả về


async def main(loop):
    all_done = asyncio.Future()

    print('scheduling mark_done')  # 1
    loop.call_soon(mark_done, all_done, 'the result')

    result = await all_done
    print('returned result: {!r}'.format(result))  # 3


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main(event_loop))
    finally:
        event_loop.close()

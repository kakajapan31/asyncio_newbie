import asyncio
import functools


def callback(future, n):  # Được gọi sau khi future done, set_result
    print('{}: future done: {}'.format(n, future.result()))  # 3


async def register_callbacks(all_done):
    print('registering callbacks on future')  # 1
    all_done.add_done_callback(functools.partial(callback, n=1))  # Thêm callback
    all_done.add_done_callback(functools.partial(callback, n=2))  # Thêm callback


async def main(all_done):
    await register_callbacks(all_done)
    print('setting result of future')  # 2
    all_done.set_result('the result')


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        all_done = asyncio.Future()
        event_loop.run_until_complete(main(all_done))
    finally:
        event_loop.close()

import asyncio


async def phase1():
    print('in phase1')  # 3
    await asyncio.sleep(2)
    print('done with phase1')  # 6
    return 'phase1 result'


async def phase2():
    print('in phase2')  # 4
    await asyncio.sleep(1)
    print('done with phase2')  # 5
    return 'phase2 result'


async def main():
    print('starting main')  # 1
    print('waiting for phases to complete')  # 2
    results = await asyncio.gather(
        phase1(),
        phase2(),
    )  # gom các task lại thành 1 nhóm và run async chúng
    print('results: {!r}'.format(results))  # 7


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main())
    finally:
        event_loop.close()

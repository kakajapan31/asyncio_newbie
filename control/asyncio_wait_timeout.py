import asyncio


async def phase(i):
    print('in phase {}'.format(i))  # 3, 4, 5
    try:
        await asyncio.sleep(0.1 * i)
    except asyncio.CancelledError:
        print('phase {} canceled'.format(i))  # 10, 11
        raise
    else:
        print('done with phase {}'.format(i))  # 6
        return 'phase {} result'.format(i)


async def main(num_phases):
    print('starting main')  # 1
    phases = [
        phase(i)
        for i in range(num_phases)
    ]
    print('waiting 0.1 for phases to complete')  # 2
    completed, pending = await asyncio.wait(phases, timeout=0.1)  # Gọi các task trong 0.1s, task nào xong thì trả vào
                                                                  # complete, chưa xong thì ở pending
    print('{} completed and {} pending'.format(
        len(completed), len(pending),
    ))  # 7
    # Cancel remaining tasks so they do not generate errors
    # as we exit without finishing them.
    if pending:
        print('canceling tasks')  # 8
        for t in pending:
            t.cancel()  # cancel các task pending
    print('exiting main')  # 9


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main(3))
    finally:
        event_loop.close()

import asyncio


async def phase(i):
    print('in phase {}'.format(i))  # 3, 4, 5
    await asyncio.sleep(0.5 - (0.1 * i))  # sleep và nhường tài nguyên, i càng nhỏ thì thời gian lấy lại càng lâu
    print('done with phase {}'.format(i))  # 6, 8, 10
    return 'phase {} result'.format(i)


async def main(num_phases):
    print('starting main')  # 1
    phases = [
        phase(i)
        for i in range(num_phases)
    ]
    print('waiting for phases to complete')  # 2
    results = []
    for next_to_complete in asyncio.as_completed(phases):  # chạy async list phase (không còn theo thứ tự)
        answer = await next_to_complete  # Đợi run hàm phase xong
        print('received answer {!r}'.format(answer))  # 7, 9, 11
        results.append(answer)
    print('results: {!r}'.format(results))  # 12
    return results


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main(3))
    finally:
        event_loop.close()

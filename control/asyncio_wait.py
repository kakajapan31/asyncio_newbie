import asyncio


async def phase(i):
    print('in phase {}'.format(i))  # 3, 4, 5
    await asyncio.sleep(0.1 * i)
    print('done with phase {}'.format(i))  # 6, 7, 8
    return 'phase {} result'.format(i)


async def main(num_phases):
    print('starting main')  # 1
    phases = [
        phase(i)
        for i in range(num_phases)
    ]
    print('waiting for phases to complete')  # 2
    completed, pending = await asyncio.wait(phases)  # chờ cho đến khi tất cả các phases đều hoàn tất
    results = [t.result() for t in completed]
    print('results: {!r}'.format(results))  # 9


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main(3))
    finally:
        event_loop.close()

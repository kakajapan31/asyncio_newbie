# Các lệnh print sẽ in theo thứ tự từ thấp đến cao

import asyncio


async def coroutine():
    print('in coroutine')  # 3


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()  # tạo 1 event loop run async
    try:
        print('starting coroutine')  # 1
        coro = coroutine()
        print('entering event loop')  # 2
        event_loop.run_until_complete(coro)  # run cho đến khi coro hoàn thành
    finally:
        print('closing event loop')  # 4
        event_loop.close()

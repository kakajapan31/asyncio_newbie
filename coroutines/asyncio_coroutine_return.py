import asyncio


async def coroutine():
    print('in coroutine')  # 1
    return 'result'  # return kết quả của func


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        return_value = event_loop.run_until_complete(
            coroutine()
        )  # nhận kết quả của func sau khi run xong
        print('it returned: {!r}'.format(return_value))  # 2
    finally:
        event_loop.close()

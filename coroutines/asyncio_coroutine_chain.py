import asyncio


async def outer():
    print('in outer')  # 1
    print('waiting for result1')  # 2
    result1 = await phase1()  # await sẽ đợi cho đến khi hàm phase1 hoàn thành
    print('waiting for result2')  # 4
    result2 = await phase2(result1)
    return result1, result2


async def phase1():
    print('in phase1')  # 3
    return 'result1'


async def phase2(arg):
    print('in phase2')  # 5
    return 'result2 derived from {}'.format(arg)


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        return_value = event_loop.run_until_complete(outer())
        print('return value: {!r}'.format(return_value))  # 6
    finally:
        event_loop.close()

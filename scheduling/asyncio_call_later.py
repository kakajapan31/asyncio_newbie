import asyncio


def callback(n):
    print('callback {} invoked'.format(n))  # 3


async def main(loop):
    print('registering callbacks')  # 2
    loop.call_later(0.2, callback, 1)  # call sau 0.2s
    loop.call_later(0.1, callback, 2)  # call sau 0.1s
    loop.call_soon(callback, 3)  # call ngay

    await asyncio.sleep(2)  # ngủ 2s và nhường cho coroutine khác

if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        print('entering event loop')  # 1
        event_loop.run_until_complete(main(event_loop))
    finally:
        print('closing event loop')  # 4
        event_loop.close()

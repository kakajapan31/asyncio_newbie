import asyncio
import functools


def callback(arg, *, kwarg='default'):
    print('callback invoked with {} and {}'.format(arg, kwarg))  # 4


async def main(loop):
    print('registering callbacks')  # 2
    loop.call_soon(callback, 1)  # call ngay sau khi có await
    wrapped = functools.partial(callback, kwarg='not default')
    loop.call_soon(wrapped, 2)  # call ngay
    print('123')  # 3

    await asyncio.sleep(0.1)


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        print('entering event loop')  # 1
        event_loop.run_until_complete(main(event_loop))
    finally:
        print('closing event loop')  # 5
        event_loop.close()

import asyncio
import time


def callback(n, loop):
    print('callback {} invoked at {}'.format(n, loop.time()))  # 5


async def main(loop):
    now = loop.time()
    print('clock time: {}'.format(time.time()))  # 2
    print('loop  time: {}'.format(now))  # 3

    print('registering callbacks')  # 4
    loop.call_at(now + 0.2, callback, 1, loop)  # call func callback sau 0.2s
    loop.call_at(now + 0.1, callback, 2, loop)  # call sau 0.1s
    loop.call_soon(callback, 3, loop)  # call ngay khi có tín hiệu await bắt đầu

    await asyncio.sleep(1)


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        print('entering event loop')  # 1
        event_loop.run_until_complete(main(event_loop))
    finally:
        print('closing event loop')  # 6
        event_loop.close()

import asyncio
import functools


def set_event(event):
    print('setting event in callback')  # 3
    event.set()


async def coro1(event):
    print('coro1 waiting for event')  # 2
    await event.wait()
    print('coro1 triggered')  # 4


async def coro2(event):
    print('coro2 waiting for event')  # 2
    await event.wait()
    print('coro2 triggered')  # 4


async def main(loop):
    # Create a shared event
    event = asyncio.Event()
    print('event start state: {}'.format(event.is_set()))  # 1 False

    loop.call_later(
        0.1, functools.partial(set_event, event)
    )

    await asyncio.wait([coro1(event), coro2(event)])
    print('event end state: {}'.format(event.is_set()))  # 5 True


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main(event_loop))
    finally:
        event_loop.close()

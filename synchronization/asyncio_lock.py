import asyncio
import functools


def unlock(lock):
    print('callback releasing lock')  # 6
    lock.release()


async def coro1(lock):
    print('coro1 waiting for the lock')  # 4
    async with lock:
        print('coro1 acquired lock')  # 7
    print('coro1 released lock')  # 8


async def coro2(lock):
    print('coro2 waiting for the lock')  # 5
    await lock.acquire()
    try:
        print('coro2 acquired lock')
    finally:
        print('coro2 released lock')
        lock.release()


async def main(loop):
    # Create and acquire a shared lock.
    lock = asyncio.Lock()
    print('acquiring the lock before starting coroutines')  # 1
    await lock.acquire()
    print('lock acquired: {}'.format(lock.locked()))  # 2

    # Schedule a callback to unlock the lock.
    loop.call_later(1, functools.partial(unlock, lock))

    # Run the coroutines that want to use the lock.
    print('waiting for coroutines')  # 3
    await asyncio.wait([coro1(lock), coro2(lock)]),


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main(event_loop))
    finally:
        event_loop.close()

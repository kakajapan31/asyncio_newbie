import asyncio


async def task_func():
    print('in task_func, sleeping')  # 2
    try:
        await asyncio.sleep(1)  # task tạm dừng trong 1s, nhường cho task khác (1)
    except asyncio.CancelledError:  # task đã bị hủy bởi (2)
        print('task_func was canceled')  # 5
        raise
    return 'the result'


def task_canceller(t):  # task sau khi run lệnh (1) sẽ tiếp tục ở đây
    print('in task_canceller')  # 3
    t.cancel()  # hủy task  (2)
    print('canceled the task')  # 4


async def main(loop):
    print('creating task')  # 1
    task = loop.create_task(task_func())  # Tạo task
    loop.call_soon(task_canceller, task)  # Gọi hàm task_cancel với tham số task
    try:
        await task  # đợi chờ task hoàn thành
    except asyncio.CancelledError:  # task bị hủy, exception bắt lỗi
        print('main() also sees task as canceled')  # 6


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main(event_loop))
    finally:
        event_loop.close()

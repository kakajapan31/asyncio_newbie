# Các lệnh print sẽ in theo thứ tự từ thấp đến cao

import asyncio


async def task_func():
    print('in task_func')  # 3
    return 'the result'  # 3.


async def main(loop):
    print('creating task')  # 1
    task = loop.create_task(task_func())  # Tạo 1 task mới
    print('waiting for {!r}'.format(task))  # 2
    return_value = await task  # Tại đây, task mới bắt đầu được chạy, await sẽ đợi task chạy xong
    print('task completed {!r}'.format(task))  # 4
    print('return value: {!r}'.format(return_value))  # 5


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main(event_loop))
    finally:
        event_loop.close()

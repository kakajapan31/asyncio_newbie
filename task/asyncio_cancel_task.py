import asyncio


async def task_func():
    print('in task_func')
    return 'the result'


async def main(loop):
    print('creating task')  # 1
    task = loop.create_task(task_func())  # Task được tạo

    print('canceling task')  # 2
    task.cancel()  # Task bị cancel

    print('canceled task {!r}'.format(task))  # 3
    try:
        await task  # Chờ cho tới khi task hoàn thành
    except asyncio.CancelledError:  # Đợi hoài không thấy
        print('caught error from canceled task')  # 4
    else:
        print('task result: {!r}'.format(task.result()))


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main(event_loop))
    finally:
        event_loop.close()

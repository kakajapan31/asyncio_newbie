import asyncio


async def wrapped():
    print('wrapped')  # 6
    return 'result'


async def inner(task):
    print('inner: starting')  # 4
    print('inner: waiting for {!r}'.format(task))  # 5
    result = await task
    print('inner: task returned {!r}'.format(result))  # 7


async def starter():
    print('starter: creating task')  # 2
    task = asyncio.ensure_future(wrapped())  # wrap task lại
    print('starter: waiting for inner')  # 3
    await inner(task)
    print('starter: inner returned')  # 8


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        print('entering event loop')  # 1
        result = event_loop.run_until_complete(starter())
    finally:
        event_loop.close()

# asyncio_newbie

### 30-31/3: 
- Code các example có sẵn liên quan đến task, coroutines, gather, call function async, xem cách thức hoạt động từ các ví dụ
- Hiểu được task, coroutine gather hoạt động thông qua các ví dụ

1. Coroutines:
    - [coroutines](coroutines/asyncio_coroutine.py)
    - [coroutines-return](coroutines/asyncio_coroutine_return.py)
    - [chain](coroutines/asyncio_coroutine_chain.py)

1. Task:
    - [create_task](task/asyncio_create_task.py)
    - [cancel_task](task/asyncio_cancel_task.py)
    - [cancel_task](task/asyncio_cancel_task2.py)
    - [ensure_future](task/asyncio_ensure_future.py): wrap task lại 

1. Call Scheduling
    - [Call-at](scheduling/asyncio_call_at.py)
    - [call-later](scheduling/asyncio_call_later.py)
    - [call-soon](scheduling/asyncio_call_soon.py)
    
1. Control
    - [as-completed](control/asyncio_as_completed.py)
    - [gather](control/asyncio_gather.py)
    - [wait](control/asyncio_wait.py)
    - [wait-timeout](control/asyncio_wait_timeout.py)

1. Future
    - [await](producing_result/asyncio_future_await.py)
    - [callback](producing_result/asyncio_future_callback.py)
    - [event-loop](producing_result/asyncio_future_event_loop.py)
    
### 01-02/4:
- Tìm hiểu và code các ví dụ liên quan đến Condition, Event, Lock, Queue
- Viết báo cáo :D, note các ghi chú vào những file task, coroutine, gather, future
- Hiểu các hoạt động của condition, event, lock, queue từ ví dụ

1. Synchonization:
    - [Condition](synchronization/asyncio_condition.py)
    - [Event](synchronization/asyncio_event.py)
    - [Lock](synchronization/asyncio_lock.py)
    - [Queue](synchronization/asyncio_queue.py)

1. IO_Protocol: liên quan đến Stream, cái này chưa tìm hiểu kĩ :D

### 03-04/4, Tìm hiểu về Stream: io_protocol, io_coroutine, socket